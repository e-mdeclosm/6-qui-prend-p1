#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "fonctions.h"
#define N 104

void regles(){ //fonction qui affiche les regles du jeu avec fichier csv
  int n=1000;char rep;
  printf("Voulez-vous d'abord connaître les règles du jeu? (o/n)\n");
  scanf("%c",&rep);
  if (rep == 'o'){
    FILE* mesJeux = NULL; char ligne[n];
    int nbLignesLues = 0; int nbColonnesLues = 0;char* laLigne;
    laLigne=malloc(sizeof(char)*n);
    mesJeux = fopen("regles.csv", "r");
    if (mesJeux == NULL) { //Si erreur lors de l'ouverture du fichier
      printf("Erreur ouverture fichier \n"); //Ecrire 'erreur'
    }else{
      while(fgets(ligne,n,mesJeux) !=NULL){
        laLigne= strtok(ligne," ");
        while (laLigne != NULL){
          printf("%s ",laLigne); //Lecture de chaque ligne
          laLigne = strtok(NULL, ";");
          nbColonnesLues++;}
        printf("\n");
        nbColonnesLues=0;
        nbLignesLues++;}
      fclose(mesJeux);
    }}
  else{
    printf("Parfait jouons\n\n");}
}

void emptyBuffer() { //Vide le buffer après une saisie
  char c;
  while (((c = getchar()) != '\n') && (c != EOF));
}

void creation_cartes(Carte pile[104]) {//crée les 104 cartes de jeu et les têtes de boeufs correspondantes
  for (int j = 0; j < 104; j++) {            //par défault les cartes ont 1 tdb
        pile[j].tdb = 1;}
    for (int i = 0; i < 104; i++) {
        pile[i].nb = i + 1;
        if (pile[i].nb % 5 == 0 && pile[i].nb % 10 != 0) {  //si nombre divisible par 5 alors tdb= 2
            pile[i].tdb = 2;}
        if (pile[i].nb % 10 == 0) {  //si nombre divisible par 10 alors tdb = 3
            pile[i].tdb = 3;}
        if (pile[i].nb % 11 == 0 && pile[i].nb != 55) { //si nombre divisible par 11 alors tdb = 5
            pile[i].tdb = 5;}
        if (pile[i].nb == 55) { //si nombre = 55 alors tdb = 7
            pile[i].tdb = 7;
        }
    }
}

void shuffle(Carte pile[104]) { //fonction qui mélange les cartes
    srand(time(NULL));

    for (int i = 0; i < 104; i++) {
        int j = rand() % 104;

        Carte temp = pile[i];
        pile[i] = pile[j];
        pile[j] = temp;
    }}


void nbjoueurs(int *nb_joueurs){ //Fonction qui demande le nombre de participants
  int joueurs_reels=0;
  int scanfResult;
  do{
    printf("Combien de joueurs (réels) veulent jouer? (1-10): ");
    scanfResult=scanf("%d",&joueurs_reels);
    emptyBuffer(); //Vidage du tampon//
  }while(scanfResult != 1 || joueurs_reels<=0 || joueurs_reels>10);
  printf("C'est parti, jouons!\n");
  *nb_joueurs=joueurs_reels;
}

void creationjoueur(Joueur* tabJoueurs,int nb_joueurs){ //Fonction pour crée chaque joueur en demandant son nom//
    for (int i=0;i<nb_joueurs;i++){
        tabJoueurs[i].nom=malloc(20*sizeof(char));
        printf("Quel est votre nom ? \n");
        scanf("%s",tabJoueurs[i].nom);

        tabJoueurs[i].jeu=malloc(sizeof(Carte)*10);
        tabJoueurs[i].carte_ramassee=malloc(sizeof(Carte)*20);
        tabJoueurs[i].points=0;
    }
}

void distribuer_cartes(Carte pile[104], int nb_joueurs, Joueur* tabJoueurs) {
    int indice_carte = 0; //distribution des cartes par rapport au nombre de joueur

    for (int i = 0; i < nb_joueurs; i++){
        for (int j = 0; j < 10; j++) {
            tabJoueurs[i].jeu[j] = pile[indice_carte]; //on attribue pour chaque jeu de joueur 10 cartes de la pile
            indice_carte++;
        }
    }
}

void echanger_carte_joueurs(Joueur joueur, int i, int j){
  Carte temp = joueur.jeu[i];
  joueur.jeu[i]=joueur.jeu[j];
  joueur.jeu[j]=temp;
}

void trier_cartes(int nb_joueurs,Joueur* tabJoueurs){
  int indice = 0;
  do{
    int min;
    for( int i=0; i<9;i++){
      min=i;
      for (int j=i+1; j<10;j++){
        if (tabJoueurs[indice].jeu[j].nb < tabJoueurs[indice].jeu[min].nb){
          min =j;
        }
      }
    echanger_carte_joueurs(tabJoueurs[indice],i,min);
    }
    indice++;
  }while(indice<nb_joueurs);
}

void initialiser_plateau(Carte pile[104], Carte plateau[4][6]) {
  int indice_carte = 103;
  for(int i=0;i<4;i++){
    for(int j=0;j<6;j++){
      plateau[i][j].nb=0;
    }
  }
  for (int i=0;i<4;i++){
    plateau[i][0]= pile[indice_carte];
    indice_carte--;
  }
}
void aff_plateau(Carte plateau[4][6],int i, int j){
  printf("  ["); //Affiche le n° de la carte et le nb de
  if (plateau[i][j].nb < 10) {
    printf("  ");
  }
  if (plateau[i][j].nb > 9 && plateau[i][j].nb < 100){
    printf(" ");
  }
  printf("%d,",plateau[i][j].nb);
  for(int k=0;k<plateau[i][j].tdb;k++){
    printf("🐮");
  }
  printf("]  ");
  for(int h=0;h<14-(plateau[i][j].tdb*2);h++){
    printf(" ");
  }
  printf("||");
}

void plateau_de_jeu(Carte plateau[4][6]){ //fonction qui affiche les cartes du plateau
  for(int i=0;i<4;i++){
    for (int j=0;j<6;j++){
      if(plateau[i][j].nb == 0){
        printf("          __            ||"); //Si la case du plateau est composée d'un 0 alors on affiche __ à la place
      }
      else{
        aff_plateau(plateau,i,j);
      }
    }
    printf("\n");
  }
}

void affichage_main_joueurs(Joueur joueur,int nb_tour){ //Fonction qui aide à l'affichage (économie de lignes)
  for (int j=0;j<nb_tour;j++){
    if(joueur.jeu[j].nb!=0){
      printf("----------------   ");
    }}
    printf("\n");
}

void aff_main_joueurs(int nb_tour,Joueur joueur){
  for (int j=0;j<nb_tour;j++){
    if(joueur.jeu[j].nb!=0){
      printf("|");
      for (int f=0;f<(14-joueur.jeu[j].tdb*2)/2;f++){
        printf(" ");}
      for (int h=0;h<joueur.jeu[j].tdb;h++){
        printf("🐮");} //Affiche le nb de tdb de chaque carte
      for (int f=0;f<(14-joueur.jeu[j].tdb*2)/2;f++){
        printf(" ");}
      printf("|   ");
    }}
}

void main_joueurs(Joueur joueur,int nb_tour){ //Fonciton qui affiche les cartes pour chaque joueur
  printf("\n\n%s, voici vos cartes:\n",joueur.nom);
  for (int i=0;i<nb_tour;i++){
    printf("Carte n°%d:         ",i+1);
  }
  printf("\n");
  affichage_main_joueurs(joueur,nb_tour); //Affiche le haut de chaque carte
  for (int j=0;j<nb_tour;j++){
    printf("|     ");
    if(joueur.jeu[j].nb!=0){
      if (joueur.jeu[j].nb < 10) {
        printf("  ");}
      if (joueur.jeu[j].nb > 9 && joueur.jeu[j].nb < 100){
        printf(" ");}
      printf("%d      ",joueur.jeu[j].nb);} //Affiche le n° de chaque carte
      printf("|   ");}
    printf("\n");
    aff_main_joueurs(nb_tour,joueur);
  printf("\n");
  affichage_main_joueurs(joueur,nb_tour); //Affiche le bas de chaque carte
}

void echanger_carte_a_jouer(Carte_a_jouer* tabCartes_a_jouer,int i,int j){
  Carte_a_jouer temp = tabCartes_a_jouer[i];
  tabCartes_a_jouer[i]=tabCartes_a_jouer[j];
  tabCartes_a_jouer[j]=temp;

}

void trier_cartes_a_jouer(int nb_joueurs, Carte_a_jouer* tabCartes_a_jouer){
  int min;
  for(int i=0 ; i<nb_joueurs-1;i++){
      min=i;
      for (int j=i+1; j<nb_joueurs ; j++){
          if (tabCartes_a_jouer[j].carte.nb < tabCartes_a_jouer[min].carte.nb){
              min =j;
          }
      }
      echanger_carte_a_jouer(tabCartes_a_jouer,i,min );
  }
}

void afficher_cartes(int nb_joueurs,Carte_a_jouer* tabCartes_a_jouer,Joueur* tabJoueurs){
  printf("------ Cartes choisies par les joueurs ------\n");
  for (int i=0; i<nb_joueurs;i++){
    printf("Cartes de %s : [%d,%d]\n",tabJoueurs[tabCartes_a_jouer[i].id].nom, tabCartes_a_jouer[i].carte.nb, tabCartes_a_jouer[i].carte.tdb);
  }
}

void choisir_serie(Carte carte, Carte plateau[4][6],Joueur joueur, int nb_joueurs){
  int serie;
  plateau_de_jeu(plateau);
  printf( "%s : votre carte ne peut pas être placée car elle est trop faible !\nVeuillez entrer le numéro d'une des 4 séries sur le plateau pour y ramasser toutes les cartes !\n",joueur.nom);
  scanf("%d", &serie);
  int i=0;
  while ((joueur.carte_ramassee[i].nb )!= 0) {
    i=i+1;
  }
  int j=0;
  while (j<6 && plateau[serie-1][j].nb != 0) {
    joueur.carte_ramassee[i] = plateau[serie-1][j];
    plateau[serie-1][j].nb = 0;
    j=j+1;
  }
  plateau[serie-1][0] = carte;
}

void placer_carte(Carte_a_jouer carte_a_jouer, Carte plateau[4][6],Joueur* joueur, int nb_joueurs){
  int difference = 104;int colonnes[4];int ligne;Carte dernieres_cartes[4];
  for (int i = 0; i<4; i++) {
    int j = 0;int petit = 0;
    while (j<6 && plateau[i][j].nb != 0 && petit == 0) {
      if (carte_a_jouer.carte.nb - plateau[i][j].nb < 0) {
        dernieres_cartes[i].nb = 0;
        petit = 1;
      } else {
        dernieres_cartes[i] = plateau[i][j];
        colonnes[i] = j+1;
        j=j+1;  }}}
  for (int i = 0; i<4; i++) {
    if(carte_a_jouer.carte.nb - dernieres_cartes[i].nb < difference) {
      difference = carte_a_jouer.carte.nb - dernieres_cartes[i].nb;
      ligne = i;}}
  if (difference != carte_a_jouer.carte.nb) {
    int i = ligne;int j = colonnes[ligne];
    plateau[i][j] = carte_a_jouer.carte;
  } else {
    choisir_serie(carte_a_jouer.carte, plateau, joueur[carte_a_jouer.id], nb_joueurs);  }}

void lignecomplete(Carte_a_jouer carte,Carte plateau[4][6],Joueur* joueur){ //Balaye les lignes du plateau pour savoir si la 6ème colonne est complétée !
  for(int i=0; i<4;i++){
    if(plateau[i][5].nb!=0){ //Recherche à partir de quel indice du tableau carte_ramassée_joueur le tableau n'est pas complété
      int k=0;
      while (joueur[carte.id].carte_ramassee[k].nb!= 0) {
        k=k+1;} //met les élément du tableau dans les cartes ramassées//
      for( int h=0;h<5;h++){
        joueur[carte.id].carte_ramassee[k+h]=plateau[i][h];
      }
      printf("La ligne n° %d est pleine! %s tu récupère les cartes de la ligne !\n",i+1,joueur[carte.id].nom); //remet la ligne à 0 et la 6ème élement au début de la ligne du plateau//
      plateau[i][0]=plateau[i][5];
      for (int j=1;j<6;j++){
        plateau[i][j].nb=0;
      }
    }
  }
}

void supprime_elements_similaires(Joueur* tabJoueurs,int nb_joueurs, int nb_tour){
  for (int i = 0; i < nb_joueurs; i++) { //Fonction qui supprime de la main du joueur la carte qui vient d'être jouée
    for (int k = 0; k < nb_joueurs; k++){
      if (tabJoueurs[i].jeu[k].nb == tabJoueurs[i].carte_choisie.nb && tabJoueurs[i].jeu[k].tdb == tabJoueurs[i].carte_choisie.tdb){
        for (int h = k; h < nb_tour - 1; h++) {
          tabJoueurs[i].jeu[h] = tabJoueurs[i].jeu[h+1];
        }
        tabJoueurs[i].jeu[nb_tour -1].nb=0;
        tabJoueurs[i].jeu[nb_tour -1].tdb=0;
        k--;
        }
      }
    }
}


void tour_de_jeu(int nb_tour_restants,int nb_joueurs,Carte plateau[4][6],Carte pile[104],Joueur* tabJoueurs,Carte_a_jouer* tabCartes_a_jouer){
  int scanfResult;
  for (int i=0;i<nb_joueurs;i++){ //fonction qui demande au joueur quelle carte veut-il jouer
    int select;
    do{
    system("clear");
    plateau_de_jeu(plateau);
      main_joueurs(tabJoueurs[i],nb_tour_restants);
      printf("\nQuelle carte sélectionnez-vous pour ce tour?\n");
      scanfResult=scanf("%d",&select);
      emptyBuffer();
    }while(scanfResult != 1 || select<1 || select>nb_tour_restants);
    tabJoueurs[i].carte_choisie=tabJoueurs[i].jeu[select-1];
    tabCartes_a_jouer[i].carte=tabJoueurs[i].carte_choisie;
    tabCartes_a_jouer[i].id=i;}
  system("clear");
  trier_cartes_a_jouer(nb_joueurs, tabCartes_a_jouer);
  afficher_cartes(nb_joueurs,tabCartes_a_jouer,tabJoueurs);
  for (int j=0;j<nb_joueurs;j++){
    placer_carte(tabCartes_a_jouer[j], plateau,tabJoueurs,nb_joueurs);
    lignecomplete(tabCartes_a_jouer[j],plateau,tabJoueurs);  }
  plateau_de_jeu(plateau);
  supprime_elements_similaires(tabJoueurs,nb_joueurs,nb_tour_restants);
  ptsjoueurs(nb_joueurs,tabJoueurs);
  printf("Nouveau tour dans 6 secondes... \n");
  sleep(6); //Pause pendant 6 secondes
}

int verifpts(int nb_joueurs,Joueur* tabJoueurs){
  for (int i=0;i<nb_joueurs;i++){
    if (tabJoueurs[i].points>65){
      return i;
    }
  }
  return 0;
}
void ptsjoueurs(int nb_joueurs,Joueur* tabJoueurs){ //Fonction qui compte le total de tête de boeuf de chaque joueur ,la fonction renvoi le numéro du joueur si celui ci est à plus de 66pts!
  int j;
  printf("\n Récapitulatif Points \n");
  for(int i=0;i<nb_joueurs;i++){
    int total=0;
    j=0;
    while(tabJoueurs[i].carte_ramassee[j].nb !=0){
      total=total + tabJoueurs[i].carte_ramassee[j].tdb;
      j++;
    }
    tabJoueurs[i].points=total;
    printf("%s : %d tête de taureaux \n",tabJoueurs[i].nom,tabJoueurs[i].points);
  }
  printf("\n");
}


