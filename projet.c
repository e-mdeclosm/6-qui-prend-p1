#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "fonctions.h"
#include <unistd.h> // Bibliothèque pour utilser sleep 


int main(){
    //VARIABLES //
  
  Carte pile[104]; //les 104 cartes dans l'ordre
  Carte plateau[4][6]; //les cartes appartenant au plateau de jeu


  int nb_joueurs=0; // nombre de joueurs;
  int nb_tour_restants=10;


  Joueur* tabJoueurs=NULL; // Tableau de Joueurs;
  Carte_a_jouer* tabCartes_a_jouer=NULL; //tableau de carte (carte choisie par les joueurs)
  



  //FONCTIONS//
  
  regles();
  nbjoueurs(&nb_joueurs); //demande combien de joueurs vont jouer
  
  //Allouer tableau dynamique
  tabJoueurs=malloc(sizeof(Joueur)*nb_joueurs); // Allouer le tableau de Joueurs en fct du nb de celui-ci
  tabCartes_a_jouer=malloc(sizeof(Carte_a_jouer)*nb_joueurs);

  creationjoueur(tabJoueurs, nb_joueurs);//Fonction qui initialise chaquejoueurs
  creation_cartes(pile); //fonction qui crée les 104 cartes et donne les tête de taureaux correspondantes

  shuffle(pile); //fonction qui mélange notre pile de cartes
  distribuer_cartes(pile, nb_joueurs, tabJoueurs); //fonction qui distribue les cartes en fonction du nombre de joueurs
  trier_cartes(nb_joueurs, tabJoueurs); //fonction trie les cartes des joueurs de façon croissante (tri par séléction)
  initialiser_plateau(pile, plateau);
  plateau_de_jeu(plateau);

  while(!(verifpts(nb_joueurs,tabJoueurs))){
    if(nb_tour_restants==0){
      nb_tour_restants=10;
      //verifpts(nb_joueurs,cartes_ramassees_joueurs,fin_de_jeu,points);
      sleep(7);
      shuffle(pile); //fonction qui mélange les 104 cartes
      distribuer_cartes(pile, nb_joueurs, tabJoueurs);
      trier_cartes(nb_joueurs, tabJoueurs); //fonction trie les cartes des joueurs de façon croissante (tri par séléction)
      initialiser_plateau(pile, plateau);//fonction qui distribue les cartes en fonction du nombre de joueurs
      tour_de_jeu(nb_tour_restants,nb_joueurs,plateau,pile,tabJoueurs,tabCartes_a_jouer);
      nb_tour_restants=nb_tour_restants-1;
    }
    else{
    tour_de_jeu(nb_tour_restants,nb_joueurs,plateau,pile,tabJoueurs,tabCartes_a_jouer);
    nb_tour_restants=nb_tour_restants-1;
    }
  }
  printf("Jeu terminé !\n");


//Libération de la mémoire //
  for (int i = 0; i < nb_joueurs; i++) {
    free(tabJoueurs[i].jeu);
    free(tabJoueurs[i].carte_ramassee);
    free(tabJoueurs[i].nom);
  }
  free(tabJoueurs);

  free(tabCartes_a_jouer);
  return 0;
}