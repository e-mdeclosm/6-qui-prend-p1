#ifndef projet.c
#define projet.c


typedef struct {  //Structure
  int nb;
  int tdb;
}Carte;


typedef struct{ //Structure Joueur
  Carte* jeu;
  Carte* carte_ramassee;
  char* nom;
  int points;
  Carte carte_choisie;


}Joueur;

typedef struct{
  Carte carte;
  int id;
}Carte_a_jouer;


/* Auteur : Declosmesnil Mathis */
/* Date :  21/05 */
/* Résumé :  Demande combien de joueurs veulent jouer */
/* Entrée(s) :  valeur Nombre de joueurs */
/* Sortie(s) :  Nombre de joueurs réels*/
void nbjoueurs(int *nb_joueurs);


/* Auteur : Declosmesnil Mathis */
/* Date :  21/05 */
/* Résumé : Creer les cartes et les tetes de boeufs correspondantes */
/* Entrée(s) :  Pile de carte ( tableau statique) */
/* Sortie(s) :   */
void creation_cartes(Carte pile[104]);


/* Auteur : Declosmesnil Mathis */
/* Date :  21/05 */
/* Résumé : Melange les cartes */
/* Entrée(s) :  Pile de carte ( tableau statique) */
/* Sortie(s) :   */
void shuffle(Carte pile[104]);


/* Auteur : Mendiburu Charles */
/* Date :  07/06 */
/* Résumé :  Crée les joueurs */
/* Entrée(s) :  Tableau de joueur, Nombre de joueurs */
/* Sortie(s) :  ... */
void creationjoueur(Joueur* tabJoueurs,int nb_joueurs);


/* Auteur : Mendiburu Charles */
/* Date :  20/05 */
/* Résumé :  Distribue les cartes aux joueurs */
/* Entrée(s) :  Tableau de carte du jeu / Nombres de joueurs / Tableau de joueurs */
/* Sortie(s) :  ... */
void distribuer_cartes(Carte pile[104], int nb_joueurs, Joueur* tabJoueurs);

/* Auteur : Gineste Thomas */
/* Date :  21/05 */
/* Résumé : Tri les cartes des joueurs */
/* Entrée(s) :  Nombres de joueurs / Tableau de joueurs */
/* Sortie(s) :   */
void trier_cartes(int nb_joueurs,Joueur* tabJoueurs);

/* Auteur : Declosmesnil Mathis */
/* Date :  21/05 */
/* Résumé : Supprime les cartes joués par les joueurs */
/* Entrée(s) :  Joueur / entier i / entier j */
/* Sortie(s) :   */
void echanger_carte_joueurs(Joueur joueur, int i, int j);

/* Auteur : Temple-Boyer Simon */
/* Date :  21/05 */
/* Résumé : Initialise le tableau de jeu */
/* Entrée(s) :  Tableau de carte mélangé / Plateau */
/* Sortie(s) :   */
void initialiser_plateau(Carte melange[104], Carte plateau[4][6]);


/* Auteur : Temple-Boyer Simon */
/* Date :  21/05 */
/* Résumé : Initialise le tableau de jeu */
/* Entrée(s) :  Plateau */
/* Sortie(s) :   */
void plateau_de_jeu(Carte plateau[4][6]);

/* Auteur : Gineste thomas */
/* Date :  21/05 */
/* Résumé : Affiche la main de chaque joueur */
/* Entrée(s) :  Joueur / Nombre de tour restants */
/* Sortie(s) :   */
void main_joueurs(Joueur joueur,int nb_tour);

/* Auteur : Gineste thomas */
/* Date :  21/05 */
/* Résumé : Affiche la main de chaque joueur */
/* Entrée(s) :  Joueur / Nombres de tours restants */
/* Sortie(s) :   */
void affichage_main_joueurs(Joueur joueur,int nb_tour);


/* Auteur : Mendiburu Charles */
/* Date :  12/06 */
/* Résumé : Tri les cartes des joueurs */
/* Entrée(s) :  Nombre de joueurs / Tableau de cartes à jouer */
/* Sortie(s) :  ... */
void trier_cartes_a_jouer(int nb_joueurs, Carte_a_jouer* cartes_a_jouer);

/* Auteur : Mendiburu Charles */
/* Date :  12/06 */
/* Résumé : Tri les cartes des joueurs */
/* Entrée(s) :  Tableau de cartes à jouer / entier i / entier j */
/* Sortie(s) :  ... */

void echanger_carte_a_jouer(Carte_a_jouer* tabCartes_a_jouer,int i,int j);


/* Auteur : Temple_Boyer Simon */
/* Date :  13/06 */
/* Résumé :  Affiche les cartes jouées par les joueurs */
/* Entrée(s) :  Nombre de joueurs/Tableau de cartes à jouer / Tableau de joueurs */
/* Sortie(s) :  ... */
void afficher_cartes(int nb_joueurs,Carte_a_jouer* tabCartes_a_jouer,Joueur* tabJoueurs);

/* Auteur : Mendiburu Charles */
/* Date :  15/06 */
/* Résumé : Place la carte sur le plateau de jeu*/
/* Entrée(s) :  Carte à jouer / Plateau / Tableau de Joueurs/ Nombre de joueurs/ */
/* Sortie(s) :  ... */
void placer_carte(Carte_a_jouer carte_a_jouer, Carte plateau[4][6],Joueur* joueur, int nb_joueurs);

/* Auteur : Declosmesnil Mathis */
/* Date :  15/06 */
/* Résumé : En cas de carte trop faible, choisi quelle ligne remplacer*/
/* Entrée(s) :  Carte / Plateau / Joueur / Nombre de joueurs */
/* Sortie(s) :  ... */
void choisir_serie(Carte carte, Carte plateau[4][6],Joueur joueur, int nb_joueurs);

/* Auteur : Mendiburu Charles */
/* Date :  16/06 */
/* Résumé : ramasse les cartes quand la ligne est complete*/
/* Entrée(s) :  carte à jouer / Plateau / Tableau de joueur */
/* Sortie(s) :  ... */
void lignecomplete(Carte_a_jouer carte,Carte plateau[4][6],Joueur* joueur);

/* Auteur : Mendiburu Charles */
/* Date :  17/06 */
/* Résumé : Supprime les elements lorsqu'ils ont été joué */
/* Entrée(s) :  Tableau de Joueurs / Nombre de joueurs/ Nombre de tours restants */
/* Sortie(s) :  ... */

void supprime_elements_similaires(Joueur* tabJoueurs, int nb_joueurs, int nb_tour);

/* Auteur : Declosmesnil Mathis */
/* Date :  18/06 */
/* Résumé :  Simule un tour de jeu */
/* Entrée(s) :  Nombre de tours restants/ Nombre de joueurs / Plateau / Pile de carte/ Tableau de joueurs /Tableau de carte_a_jouer */
/* Sortie(s) :  ... */
void tour_de_jeu(int nb_tour_restants,int nb_joueurs,Carte plateau[4][6],Carte pile[104],Joueur* tabJoueurs,Carte_a_jouer* tabCartes_a_jouer);



/* Auteur : Mendiburu Charles */
/* Date :  20/06 */
/* Résumé : Affiche les points des joueurs*/
/* Entrée(s) :  Nombre de joueurs/ Tableau de joueurs */
/* Sortie(s) :  ... */
void ptsjoueurs(int nb_joueurs,Joueur* tabJoueurs);

/* Auteur : Mendiburu Charles */
/* Date :  20/06 */
/* Résumé : Verifie si un joueur à gagné*/
/* Entrée(s) :  Nombre de joueurs/Tableau de joueurs */
/* Sortie(s) :  ... */
int verifpts(int nb_joueurs,Joueur* tabJoueurs);
#endif
