Programme réalisé par :

- Mathis Declosmesnil
- Thomas Gineste
- Charles Mendiburu
- Simon Temple-Boyer

Description du programme :

Ce programme offre la possibilité de jouer une partie du célèbre jeu de société "6 qui prend". Les règles et la jouabilité sont identiques au jeu d'origine bien que l'affichage est adapté à un interface où uniquement des caractères peuvent apparaître.

Contenu : 

    Voici les fichiers dont a besoin le programme pour fonctionner :

        - fonctions.c : c'est le fichier qui contient le code en c de toutes les fonctions du programme
        
        - fonctions.h : c'est le fichier qui contient la structure de toutes les fonctions présentes dans le fichier fonctions.c

        - makefile : c'est le fichier qui permet de compiler le programme

        - projet.c : c'est le fichier qui contient les variables principales et l'appel des fonctions

        - regles.csv : c'est le fichier qui contient les règles du jeu "6 qui prend" que nous utilisons dans le programme.

Etapes pour exécuter le programme :

    1) Télécharger tous les fichiers et les déplacer dans un même dossier

    2) Ouvrir le terminal

    3) Se déplacer dans le dossier (cd [nom du dossier])

    4) Taper "make" pour compiler le programme

    5) Taper "./exe" pour l'exécuter

    6) Suivre les instructions du programme et jouer au jeu comme vous le souaitez


Aides et Conseils :

    1) Mettre le terminal en plein écran pour offir une plus grande visibilité et une meilleure condition de jeu

    2) Vous pouvez choisir le nombre de joueurs réels 